/**
 * @license
 * Copyright (c) 2014, 2019, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojanimation', 'ojs/ojrouter','knockout', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojbootstrap', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojswipeactions', 'ojs/ojlistview', 'ojs/ojbutton', 'ojs/ojmenu', 'ojs/ojoption', 'ojs/ojgauge', 'ojs/ojformlayout', 'ojs/ojpopup'],
  function (AnimationUtils, Router, ko, app, moduleUtils, Bootstrap, ArrayDataProvider) {

    var router = Router.rootInstance;

    function TontinesViewModel() {
      var self = this;
      self.router = router;
      

      this.selectedMenuItem = ko.observable("");
  
      this.thresholdValues = [{max: 33}, {max: 67}, {}];
      this.menuItemAction = function( event ) {
          this.selectedMenuItem(event.target.value);
          console.log(event.target.value);
          var sourceBtn = event.target.value;
          if(sourceBtn === 'existing') {
            self.router.go("tontines-search");
          } else {
            self.router.go("tontine-form/"+event.target.value);
          }
      }.bind(this);

      // Header Config
      self.headerConfig = ko.observable({ 'view': [], 'viewModel': null });
      moduleUtils.createView({ 'viewPath': 'views/header.html' }).then(function (view) {
        self.headerConfig({ 'view': view, 'viewModel': new app.getHeaderModel() })
      })

      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      this.myItems = app.myItems;
      this.dataSource = new ArrayDataProvider(this.myItems, { idAttribute: "id" });

      this.action = ko.observable("No action taken yet");

      this.handleAction = function (event, context) {
        this.currentItem = context.data.id;
        this.doAction(event.target.value, context.data);
      }.bind(this);

      this.handleMenuItemAction = function (event) {
        this.doAction(event.target.id);
      }.bind(this);

      this.setCurrentItem = function (target) {
        var context = document.getElementById("listview").getContextByNode(target);
        if (context != null) {
          this.currentItem = context['key'];
        }
        else {
          this.currentItem = null;
        }
      }.bind(this);

      this.doAction = function (action, data) {
        if (this.currentItem != null) {
          this.action("Handle " + action + " action on: " + this.currentItem);
          
        if (action === "open") {
          self.router.go("tontine-detail/"+this.currentItem);
        }
        else if (action === "trash") {
          //if(this.currentItem.status === "stopped") {
            this.remove(this.currentItem);
          //}
        }
        else if (action === "cancel") {
          //data.status == "stopped";
          this.remove(this.currentItem);
        }
        else if (action === "more") {
          document.getElementById("moremenu").open();
        }
        else if (action === "debt") {
          this.openListener();
        }
        else if (action === "bank") {
          this.openListener();
        }
      }
      }.bind(this);

      this.remove = function (key) {
        this.myItems.remove(function (current) {
          return (current.id == key);
        });
      }.bind(this);


      this.startAnimationListener = function (event) {
        var ui = event.detail;
        if (event.target.id !== "popup1")
          return;

        if ("open" === ui.action) {
          event.preventDefault();
          var options = { "direction": "top" };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        }
        else if ("close" === ui.action) {
          event.preventDefault();
          ui.endCallback();
        }
      }.bind(this);

      this.openListener = function (event) {
        var popup = document.getElementById('popup1');
        popup.open();
      }.bind(this);

      this.cancelListener = function (event) {
        var popup = document.getElementById('popup1');
        popup.close();
      }.bind(this);


    /*
    //add to the observableArray
    self.addRow = function()
    {
       var dept = {
                     'DepartmentId': self.inputDepartmentId(),
                     'DepartmentName': self.inputDepartmentName(),
                     'LocationId': self.inputLocationId(),
                     'ManagerId': self.inputManagerId()
                  };
        self.deptObservableArray.push(dept);
    };
    
    //used to update the fields based on the selected row
    self.updateRow = function()
    {
        var element = document.getElementById('table');
        var currentRow = element.currentRow;
        
        if (currentRow != null)
        {
            self.deptObservableArray.splice(currentRow['rowIndex'], 1, {
                         'DepartmentId': self.inputDepartmentId(),
                         'DepartmentName': self.inputDepartmentName(),
                         'LocationId': self.inputLocationId(),
                         'ManagerId': self.inputManagerId()
                      });
        }
    };
    
    //used to remove the selected row
    self.removeRow = function()
    {
        var element = document.getElementById('table');
        var currentRow = element.currentRow;

        if (currentRow != null)
        {
            self.deptObservableArray.splice(currentRow['rowIndex'], 1);
        }
    };
    */





      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function () {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function () {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function () {
        // Implement if needed
      };
    }

    Bootstrap.whenDocumentReady().then(
      function () {
        ko.applyBindings(new TontinesViewModel(), document.getElementById('listviewContainer'));
      }
    );

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new TontinesViewModel();
  }
);
