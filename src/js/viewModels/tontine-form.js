/**
 * @license
 * Copyright (c) 2014, 2019, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['ojs/ojanimation', 'ojs/ojrouter', 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber',
  'ojs/ojradioset', 'ojs/ojcheckboxset',
  'ojs/ojselectcombobox', 'ojs/ojpopup',
  'ojs/ojdatetimepicker', 'ojs/ojswitch', 'ojs/ojslider',
  'ojs/ojcolorspectrum', 'ojs/ojcolorpalette', 'ojs/ojlabel',
  'ojs/ojformlayout'],
  function (AnimationUtils, Router, ko, app, moduleUtils) {

    var router = Router.rootInstance;

    function TontineFormViewModel() {
      var self = this;

      self.router = router;
      this.ref = ko.observable("");
      this.name = ko.observable("");
      this.amount = ko.observable(100);
      this.recurrence = ko.observable(1);
      this.participant = ko.observable(10);

      // Header Config
      self.headerConfig = ko.observable({ 'view': [], 'viewModel': null });
      moduleUtils.createView({ 'viewPath': 'views/header.html' }).then(function (view) {
        self.headerConfig({ 'view': view, 'viewModel': new app.getHeaderModel() })
      })

      this.btnCreate = function (event) {
        
        var data = { "id": this.ref(), "type": this.tontineType, "prct": 0, "dateDebut": getCurrentDate(), "recurrence": this.recurrence(), "dateFin": "10/10/2020", "nextTour": "", "dateNextTour": "", "participants": this.participant(), "round":1, "status": "stopped", "title": this.name(), "amount": this.amount() };
        //console.log(data);
        app.myItems.push(data);
        this.openListener();
        return true;
      }.bind(this);

      function guidGenerator() {
        var S4 = function () {
          return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4());
      }

      function getCurrentDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
          dd = '0' + dd;
        }
        if (mm < 10) {
          mm = '0' + mm;
        }
        var today = dd + '/' + mm + '/' + yyyy;
        return today;
      }

      this.startAnimationListener = function (event) {
        var ui = event.detail;
        if (event.target.id !== "popup1")
          return;

        if ("open" === ui.action) {
          event.preventDefault();
          var options = { "direction": "top" };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        }
        else if ("close" === ui.action) {
          event.preventDefault();
          ui.endCallback();
        }
      }.bind(this);

      this.openListener = function (event) {
        var popup = document.getElementById('popup1');
        popup.open('#btnGo');
      }.bind(this);

      this.cancelListener = function (event) {
        var popup = document.getElementById('popup1');
        popup.close();
        self.router.go("tontines");
      }.bind(this);


      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function () {
        var stateParams = self.router.observableModuleConfig().params.ojRouter.parameters;
        this.tontineType = stateParams.type();
        this.ref = ko.observable(guidGenerator());
        console.log(this.tontineType+" "+this.ref());
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function () {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function () {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new TontineFormViewModel();
  }
);
