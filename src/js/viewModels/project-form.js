/**
 * @license
 * Copyright (c) 2014, 2019, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['ojs/ojanimation', 'ojs/ojrouter', 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojinputnumber',
  'ojs/ojradioset', 'ojs/ojcheckboxset',
  'ojs/ojselectcombobox', 'ojs/ojpopup',
  'ojs/ojdatetimepicker', 'ojs/ojswitch', 'ojs/ojslider',
  'ojs/ojcolorspectrum', 'ojs/ojcolorpalette', 'ojs/ojlabel',
  'ojs/ojformlayout'],
  function (AnimationUtils, Router, ko, app, moduleUtils) {

    var router = Router.rootInstance;

    function ProjectFormViewModel() {
      var self = this;

      self.router = router;
      this.ref = ko.observable("");
      this.nom = ko.observable("");
      this.description = ko.observable("");
      this.categorie = ko.observable("");
      this.cost = ko.observable("");
      this.duration = ko.observable("");
      this.collecte = ko.observable("");

      // Header Config
      self.headerConfig = ko.observable({ 'view': [], 'viewModel': null });
      moduleUtils.createView({ 'viewPath': 'views/header.html' }).then(function (view) {
        self.headerConfig({ 'view': view, 'viewModel': new app.getHeaderModel() })
      })

      this.btnCreate = function (event) {
        var data = {"id": this.ref(), "duration": this.duration, "porteur": "Sanae Berrada", "nom": this.nom, "description": this.description, "categorie": this.categorie, "montant": this.cost, "collecte": "0"};
        app.myProjects.push(data);
        app.allProjects.push(data);
        this.openListener();
        return true;
      }.bind(this);

      function guidGenerator() {
        var S4 = function () {
          return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4());
      }

      function getCurrentDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
          dd = '0' + dd;
        }
        if (mm < 10) {
          mm = '0' + mm;
        }
        var today = dd + '/' + mm + '/' + yyyy;
        return today;
      }

      this.startAnimationListener = function (event) {
        var ui = event.detail;
        if (event.target.id !== "popup1")
          return;

        if ("open" === ui.action) {
          event.preventDefault();
          var options = { "direction": "top" };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        }
        else if ("close" === ui.action) {
          event.preventDefault();
          ui.endCallback();
        }
      }.bind(this);

      this.openListener = function (event) {
        var popup = document.getElementById('popup1');
        popup.open('#btnGo');
      }.bind(this);

      this.cancelListener = function (event) {
        var popup = document.getElementById('popup1');
        popup.close();
        self.router.go("projets");
      }.bind(this);


      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function () {
        this.ref = ko.observable(guidGenerator());
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function () {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function () {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new ProjectFormViewModel();
  }
);
