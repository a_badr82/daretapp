/**
 * @license
 * Copyright (c) 2014, 2019, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your detail-tontine ViewModel code goes here
 */
define(['ojs/ojrouter', 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider', 'ojs/ojnavigationlist', 'ojs/ojbutton', 'ojs/ojtoolbar', 'ojs/ojmenu'],
  function (Router, ko, app, moduleUtils, ArrayDataProvider) {

    var router = Router.rootInstance;

    function TontineDetailViewModel() {
      var self = this;

      self.router = router;
      // self.tontineDetail = {};

      this.btnReturn = function (event) {
        self.router.go("tontines");
        return true;
      }.bind(this);

      this.btnParticipants = function (event) {
        self.router.go("tontine-team");
        return true;
      }.bind(this);

      // Header Config
      self.headerConfig = ko.observable({ 'view': [], 'viewModel': null });
      moduleUtils.createView({ 'viewPath': 'views/header.html' }).then(function (view) {
        self.headerConfig({ 'view': view, 'viewModel': new app.getHeaderModel() })
      })

      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function () {

        var stateParams = self.router.observableModuleConfig().params.ojRouter.parameters;
        this.tontineId = stateParams.id();
        //console.log(stateParams.id());

        this.myItems = app.myItemsObj;
        this.tontineDetail = this.myItems.filter(tt => tt.id === this.tontineId)[0];
      };


      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function () {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function () {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new TontineDetailViewModel();
  }
);
