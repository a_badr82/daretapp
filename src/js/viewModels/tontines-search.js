/**
 * @license
 * Copyright (c) 2014, 2019, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojanimation', 'ojs/ojrouter', 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojbootstrap', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojswipeactions', 'ojs/ojlistview', 'ojs/ojbutton', 'ojs/ojmenu', 'ojs/ojoption', 'ojs/ojgauge', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojformlayout', 'ojs/ojpopup'],
  function (AnimationUtils, Router, ko, app, moduleUtils, Bootstrap, ArrayDataProvider) {

    var router = Router.rootInstance;

    function TontinesSearchViewModel() {
      var self = this;
      self.router = router;


      this.selectedMenuItem = ko.observable("");
      this.valueClearIconAlways = ko.observable("");
      this.thresholdValues = [{ max: 33 }, { max: 67 }, {}];

      // Header Config
      self.headerConfig = ko.observable({ 'view': [], 'viewModel': null });
      moduleUtils.createView({ 'viewPath': 'views/header.html' }).then(function (view) {
        self.headerConfig({ 'view': view, 'viewModel': new app.getHeaderModel() })
      })

      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      this.allItems = app.allItems;

      this.dataSource = new ArrayDataProvider(this.allItems, { idAttribute: "id" });

      this.action = ko.observable("No action taken yet");

      this.btnSearch = function (event) {
        return true;
      }.bind(this);

      this.handleAction = function (event, context) {
        this.currentItem = context.data.id;
        this.doAction(event.target.value, context.data);
      }.bind(this);

      this.handleMenuItemAction = function (event) {
        this.doAction(event.target.id);
      }.bind(this);

      this.setCurrentItem = function (target) {
        var context = document.getElementById("listview").getContextByNode(target);
        if (context != null) {
          this.currentItem = context['key'];
        }
        else {
          this.currentItem = null;
        }
      }.bind(this);

      this.doAction = function (action, data) {
        if (this.currentItem != null) {
          this.action("Handle " + action + " action on: " + this.currentItem);

          if (action === "open") {
            self.router.go("tontine-detail/" + this.currentItem);
          }
          else if (action === "trash") {
            //if(this.currentItem.status === "stopped") {
            this.remove(this.currentItem);
            //}
          }
          else if (action === "cancel") {
            data.status == "stopped";
          }
          else if (action === "submit") {
            let dataToAdd = data;
            dataToAdd.round = dataToAdd.round + 1;
            console.log(dataToAdd);
            app.myItems.push(dataToAdd);
            this.openListener();

          }
        }
      }.bind(this);

      this.startAnimationListener = function (event) {
        var ui = event.detail;
        if (event.target.id !== "popup1")
          return;

        if ("open" === ui.action) {
          event.preventDefault();
          var options = { "direction": "top" };
          AnimationUtils.slideIn(ui.element, options).then(ui.endCallback);
        }
        else if ("close" === ui.action) {
          event.preventDefault();
          ui.endCallback();
        }
      }.bind(this);

      this.openListener = function (event) {
        var popup = document.getElementById('popup1');
        popup.open('#btnGo');
      }.bind(this);

      this.cancelListener = function (event) {
        var popup = document.getElementById('popup1');
        popup.close();
        self.router.go("tontines");
      }.bind(this);



      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function () {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function () {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function () {
        // Implement if needed
      };
    }

    Bootstrap.whenDocumentReady().then(
      function () {
        ko.applyBindings(new TontinesSearchViewModel(), document.getElementById('listviewContainer'));
      }
    );

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new TontinesSearchViewModel();
  }
);
