/**
 * @license
 * Copyright (c) 2014, 2019, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your profile ViewModel code goes here
 */
define(['knockout', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojformlayout'],
 function(ko, app, moduleUtils) {

    function ProfileViewModel() {
      var self = this;
      self.userProfileModel;
      self.editMode = ko.observable(false);

      function refreshUserModel(resolve) {
        app.getUserProfile().then(function(model) {
          self.userProfileModel = model;
          if (resolve)
            resolve();
        });
      }

      // Header Config
      self.headerConfig = ko.observable({'view':[], 'viewModel':null});
      moduleUtils.createView({'viewPath':'views/header.html'}).then(function(view) {
        self.headerConfig({'view':view, 'viewModel':new app.getHeaderModel()})
      })


    // profile page header
    var leftBtnLabel = ko.computed(function(){
      return self.editMode() ? 'Back': 'Navigation Drawer';
    });

    var rightBtnLabel = ko.computed(function(){
      return self.editMode() ? 'Save': 'Edit';
    });

    var rightClickAction = function() {
      if (self.editMode()) {
        app.updateProfileData(self.userProfileModel);
        self.editMode(false);
      } else {
        self.editMode(true);
      }
    };

    var leftClickAction = function() {
      if (self.editMode()) {
        app.revertProfileData();
        refreshUserModel();
        self.editMode(false);
      } else {
        app.toggleDrawer();
      }
    };
 

      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.prefetch = function() {
        return new Promise(function(resolve, reject) {
          refreshUserModel(resolve);
        });
      }

      self.connected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new ProfileViewModel();
  }
);
