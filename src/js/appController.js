/**
 * @license
 * Copyright (c) 2014, 2019, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['knockout', 'ojs/ojmodule-element-utils', 'ojs/ojrouter', 'ojs/ojarraydataprovider', 'ojs/ojarraytreedataprovider', 'ojs/ojoffcanvas', 'ojs/ojknockouttemplateutils', 'ojs/ojknockout', 'ojs/ojmodule-element',   'ojs/ojbutton'],
  function( ko, moduleUtils, Router, ArrayDataProvider, ArrayTreeDataProvider, OffcanvasUtils, KnockoutTemplateUtils) {
     function ControllerViewModel() {
      var self = this;

      this.allItemsObj = [
        { "id": "arf71ju9", "type": "secure", "prct":0, "dateDebut":"10/10/2019", "recurrence":"1", "dateFin":"10/10/2020", "nextTour":"", "dateNextTour":"", "participants":10, "round":8, "status": "pending", "title": "Private1", "amount": "200" },
        { "id": "ae602hg6", "type": "secure", "prct":0, "dateDebut":"25/10/2019", "recurrence":"1", "dateFin":"25/10/2020", "nextTour":"", "dateNextTour":"", "participants":10, "round":5, "status": "pending", "title": "Morocco", "amount": "500" },
        { "id": "BG0y3609", "type": "secure", "prct":0, "dateDebut":"07/09/2019", "recurrence":"1", "dateFin":"07/09/2020", "nextTour":"", "dateNextTour":"", "participants":20, "round":3, "status": "pending", "title": "Rabat", "amount": "1000",  },
        { "id": "4HJG458D", "type": "secure", "prct":0, "dateDebut":"10/10/2019", "recurrence":"1", "dateFin":"10/10/2020", "nextTour":"", "dateNextTour":"", "participants":15, "round":2, "status": "pending", "title": "Oman", "amount": "350" },
        { "id": "UKD95tuh", "type": "secure", "prct":0, "dateDebut":"10/10/2019", "recurrence":"1", "dateFin":"10/10/2020", "nextTour":"", "dateNextTour":"", "participants":10, "round":5, "status": "pending", "title": "Girls", "amount": "750" },                
        { "id": "arf76ju9", "type": "secure", "prct":0, "dateDebut":"10/10/2019", "recurrence":"1", "dateFin":"10/10/2020", "nextTour":"", "dateNextTour":"", "participants":10, "round":8, "status": "pending", "title": "Oracle", "amount": "200" },
        { "id": "ae607hg6", "type": "secure", "prct":0, "dateDebut":"25/10/2019", "recurrence":"1", "dateFin":"25/10/2020", "nextTour":"", "dateNextTour":"", "participants":10, "round":5, "status": "pending", "title": "Injaz", "amount": "500" },
        { "id": "BG0y8609", "type": "secure", "prct":0, "dateDebut":"07/09/2019", "recurrence":"1", "dateFin":"07/09/2020", "nextTour":"", "dateNextTour":"", "participants":20, "round":3, "status": "pending", "title": "Private2", "amount": "1000",  },
        { "id": "4HJG958D", "type": "secure", "prct":0, "dateDebut":"10/10/2019", "recurrence":"1", "dateFin":"10/10/2020", "nextTour":"", "dateNextTour":"", "participants":15, "round":2, "status": "pending", "title": "Daret", "amount": "350" },
        { "id": "UKD9atuh", "type": "secure", "prct":0, "dateDebut":"10/10/2019", "recurrence":"1", "dateFin":"10/10/2020", "nextTour":"", "dateNextTour":"", "participants":10, "round":5, "status": "pending", "title": "Agadir", "amount": "750" },                
        { "id": "arf7bju9", "type": "secure", "prct":0, "dateDebut":"10/10/2019", "recurrence":"1", "dateFin":"10/10/2020", "nextTour":"", "dateNextTour":"", "participants":10, "round":8, "status": "pending", "title": "Marrakech", "amount": "200" },
        { "id": "ae60chg6", "type": "secure", "prct":0, "dateDebut":"25/10/2019", "recurrence":"1", "dateFin":"25/10/2020", "nextTour":"", "dateNextTour":"", "participants":10, "round":5, "status": "pending", "title": "Fez", "amount": "500" },
        { "id": "BG0yd609", "type": "secure", "prct":0, "dateDebut":"07/09/2019", "recurrence":"1", "dateFin":"07/09/2020", "nextTour":"", "dateNextTour":"", "participants":20, "round":3, "status": "pending", "title": "Meknes", "amount": "1000",  },
        { "id": "4HJGe58D", "type": "secure", "prct":0, "dateDebut":"10/10/2019", "recurrence":"1", "dateFin":"10/10/2020", "nextTour":"", "dateNextTour":"", "participants":15, "round":2, "status": "pending", "title": "Tanger", "amount": "350" },
        { "id": "UKD9ftuh", "type": "secure", "prct":0, "dateDebut":"10/10/2019", "recurrence":"1", "dateFin":"10/10/2020", "nextTour":"", "dateNextTour":"", "participants":10, "round":5, "status": "pending", "title": "Oujda", "amount": "750" },                

      ];
      this.allItems = ko.observableArray(this.allItemsObj);

      this.myItemsObj = [
        { "id": "ae609hg6", "type": "free"  , "prct":100, "dateDebut":"25/10/2019", "recurrence":"1", "dateFin":"25/10/2020", "nextTour":"Sanae", "dateNextTour":"Today", "round":3, "participants":5, "status": "started", "title": "Colleagues", "amount": "500" },
        { "id": "4HJG658D", "type": "secure", "prct":60, "dateDebut":"10/10/2019", "recurrence":"3", "dateFin":"10/10/2020", "nextTour":"Badr", "dateNextTour":"10/02/2020", "round":6, "participants":12, "status": "pending", "title": "Casablanca", "amount": "350" },
        { "id": "BG0yg609", "type": "free", "prct":45, "dateDebut":"07/09/2019", "recurrence":"2", "dateFin":"07/09/2020", "nextTour":"Ahmed", "dateNextTour":"10/02/2020", "round":7, "participants":8, "status": "stopped", "title": "Private", "amount": "1000" }
      ];
      this.myItems = ko.observableArray(this.myItemsObj);


      this.allProjectsObj = [
        { "id": "arf71ju9", "nom": "Wash up", "duration": "3 months", "porteur":"Omar Bennis", "categorie":"Social", "description":"Laundry service that enables jobless woman to exerce an income-generating activity, that is to wash student’s clothes.", "collecte":"4000", "montant":"10000"},
        { "id": "arf72ju9", "nom": "Eco Energy", "duration": "12 months", "porteur":"Sanae Berrada", "categorie":"Energy", "description":"The project consists of producing and commercializing portable refigerants by employing jobless people, to offer an alternative solution for people who don’t have refrigerants.", "collecte":"10000", "montant":"50000"},
        { "id": "arf73ju9", "nom": "Water filter", "duration": "7 months", "porteur":"Safia Achour", "categorie":"Ecology", "description":"The production and commercialisation of biological and ecological water filter, that enables people to re-use waste water. ", "collecte":"5000", "montant":"20000"},
        { "id": "arf74ju9", "nom": "Balade Bourgerag","duration": "4 months", "porteur":"Anas Boughaleb", "categorie":"Tourism", "description":"Our goal is to develop  the tourism of Oued Bouregreg by creating a small business of touristy  boat trips, we intend to build our business model by colaborationg with the existing boat’s owners in the region.", "collecte":"500", "montant":"10000"},
        { "id": "arf75ju9", "nom": "Eco friendly energy","duration": "8 months", "porteur":"Amina Dribi", "categorie":"Ecology", "description":"Our project resolves the basic problem of household’s energy that is often too expensive and does not respect the environnement, we offer a cheaper, sustainable and ecological energy with low CO2 emissions and a bigger lifetime span ", "collecte":"10000", "montant":"25000"}
       ];
      this.allProjects = ko.observableArray(this.allProjectsObj);

      this.myProjectsObj = [
        { "id": "arf72ju9", "nom": "Eco Energy", "duration": "12 months", "porteur":"Sanae Berrada", "categorie":"Energy", "description":"The project consists of producing and commercializing portable refigerants by employing jobless people, to offer an alternative solution for people who don’t have refrigerants.", "collecte":"10000", "montant":"50000"}
        ];
      this.myProjects = ko.observableArray(this.myProjectsObj);

      self.KnockoutTemplateUtils = KnockoutTemplateUtils;

      // Handle announcements sent when pages change, for Accessibility.
      self.manner = ko.observable('polite');
      self.message = ko.observable();
      self.waitForAnnouncement = false;
      self.navDrawerOn = false;

      document.getElementById('globalBody').addEventListener('announce', announcementHandler, false);

      /*
        @waitForAnnouncement - set to true when the announcement is happening.
        If the nav-drawer is ON, it is reset to false in 'ojclose' event handler of nav-drawer.
        If the nav-drawer is OFF, then the flag is reset here itself in the timeout callback.
      */
      function announcementHandler(event) {
        self.waitForAnnouncement = true;
        setTimeout(function() {
          self.message(event.detail.message);
          self.manner(event.detail.manner);
          if (!self.navDrawerOn) {
            self.waitForAnnouncement = false;
          }
        }, 200);
      };

      // Router setup
      self.router = Router.rootInstance;
      self.router.configure({
       'login': {label: 'Login', isDefault: true},
       'accueil': {label: 'Home'},
       'tontines': {label: 'My Darets'},
       'tontine-detail/{id}': {label: 'Daret Detail'},
       'tontine-form/{type}': {label: 'New Daret'},
       'tontine-team': {label: 'Participants'},
       'tontines-search': {label: 'Search a Daret'},
       'incidents': {label: 'Incidents'},
       'projets': {label: 'My Projects'},
       'project-detail/{id}': {label: 'Project Detail'},
       'project-form': {label: 'New Project'},
       'projects-search': {label: 'Contribute on a Project'},
       'profile': {label: 'Profile'},
       'about': {label: 'About'}
      });
      Router.defaults['urlAdapter'] = new Router.urlParamAdapter();

      self.moduleConfig = ko.observable({'view':[], 'viewModel':null});

      self.loadModule = function() {
        ko.computed(function() {
          var name = self.router.moduleConfig.name();
          var viewPath = 'views/' + name + '.html';
          var modelPath = 'viewModels/' + name;
          var masterPromise = Promise.all([
            moduleUtils.createView({'viewPath':viewPath}),
            moduleUtils.createViewModel({'viewModelPath':modelPath})
          ]);
          masterPromise.then(
            function(values){
              self.moduleConfig({'view':values[0],'viewModel':values[1]});
            }
          );
        });
      };

      // Navigation setup
      var navData = [
      {name: 'Home', id: 'accueil', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 fa fa-home'},
      {name: 'Darets', id: 'tontines', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 fa fa-sync'},
      {name: 'Projects', id: 'projets', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 fa fa-briefcase'},
      {name: 'Profile', id: 'profile', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
      {name: 'About', id: 'about', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-info-icon-24'}
      ];
      self.navDataProvider = new ArrayDataProvider(navData, {keyAttributes: 'id'});

      var treeNavData = [
        {name: 'Home', id: 'accueil', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 fa fa-home'},
        {name: 'Darets Space', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 fa fa-sync', 
          children: [
            {name: 'My Darets', id: 'tontines', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 fa fa-user-check'},
            {name: 'Create a new free Daret', id: 'tontine-form/free', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 fa fa-plus-circle'},
            {name: 'Search a new secure Daret', id: 'tontines-search', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 fa fa-search'}
          ]
        },
        {name: 'Projects Space', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 fa fa-briefcase',
          children: [
            {name: 'My Projects', id: 'projets', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 fa fa-user-check'},
            {name: 'Create a new project', id: 'project-form', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 fa fa-plus-circle'},
            {name: 'Contribute on a project', id: 'projects-search', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 fa fa-search'}
          ]
        },
        {name: 'Profile', id: 'profile', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
        {name: 'About', id: 'about', iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-info-icon-24'}
        ];
      self.treeNavDataProvider =  new ArrayTreeDataProvider(treeNavData, {keyAttributes: 'id'});
      // Drawer setup
      self.toggleDrawer = function() {
        return OffcanvasUtils.toggle({selector: '#navDrawer', modality: 'modal', content: '#pageContent'});
      }
      // Add a close listener so we can move focus back to the toggle button when the drawer closes
      document.getElementById('navDrawer').addEventListener("ojclose", function() { document.getElementById('drawerToggleButton').focus(); });

      // Header Setup
      self.getHeaderModel = function() {
        this.pageTitle = self.router.currentState().label;
        this.transitionCompleted = function() {
          // Adjust content padding after header bindings have been applied
          self.adjustContentPadding();
        }
        this.toggleDrawer = self.toggleDrawer;
      };

      // Method for adjusting the content area top/bottom paddings to avoid overlap with any fixed regions.
      // This method should be called whenever your fixed region height may change.  The application
      // can also adjust content paddings with css classes if the fixed region height is not changing between
      // views.
      self.adjustContentPadding = function () {
        var topElem = document.getElementsByClassName('oj-applayout-fixed-top')[0];
        var contentElem = document.getElementsByClassName('oj-applayout-content')[0];
        var bottomElem = document.getElementsByClassName('oj-applayout-fixed-bottom')[0];

        if (topElem) {
          contentElem.style.paddingTop = topElem.offsetHeight+'px';
        }
        if (bottomElem) {
          contentElem.style.paddingBottom = bottomElem.offsetHeight+'px';
        }
        // Add oj-complete marker class to signal that the content area can be unhidden.
        // See the override.css file to see when the content area is hidden.
        contentElem.classList.add('oj-complete');
      }
    }

    return new ControllerViewModel();
  }
);
